{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.gui [
  "chonker"
])
{
  environment.systemPackages = with pkgs; [
    gitAndTools.gitFull
    diffstat
    sqlitebrowser
    #unstable.nixops
    #nixops
    #nixopsUnstable
    #nixops_unstable
    #unstable.nixops_unstable
    cachix
    #nix-direnv
    gnumake
    gcc
    cmake
    ninja
    entr
    parallel
    jq

    linuxPackages.bcc
    linuxPackages.bpftrace

    (python3.withPackages(ps: with ps; [
      pyfiglet # yeah
      setuptools # unnamed dependency for pyfiglet (not so "yeah")
    ]))
  ];
}

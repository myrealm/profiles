{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.gui [
  "headless"
  "minimal"
  "office"
  "chonker"
])
{
  environment.variables = { EDITOR = "vim"; };

  environment.systemPackages = with pkgs; [
    (neovim.override {
      vimAlias = true;
      configure = {
        packages.myPlugins = with pkgs.vimPlugins; {
          #start = [ vim-lastplace vim-nix  jedi-vim deoplete-jedi deoplete-clang nerdtree vim-airline auto-pairs ];
          start = [
            vim-lastplace
            vim-nix
            jedi-vim
            deoplete-jedi
            deoplete-clang
            nerdtree
            vim-airline
            vim-yaml
          ];
          opt = [];
        };
        customRC = ''
          " the usual
          set backspace=indent,eol,start
          set backspace=2 " make backspace work like most other programs

          set viminfo='20,<1000,s1000
          set hlsearch
          set number
          " set ruler
          " Tabs are 8 characters, and thus indentations are also 8 characters.
          " There are heretic movements that try to make indentations 4 (or even 2!) characters deep,
          " and that is akin to trying to define the value of PI to be 3.
          set tabstop=8
          set softtabstop=8
          set shiftwidth=8
          set noexpandtab

          autocmd FileType php set tabstop=4 softtabstop=4 shiftwidth=4

          " 80 col width marker
          highlight ColorColumn ctermbg=Black ctermfg=DarkRed
          call matchadd('ColorColumn', '\%81v', 100)

          " Highlight trailing spaces
          " http://vim.wikia.com/wiki/Highlight_unwanted_spaces
          highlight ExtraWhitespace ctermbg=red guibg=red
          match ExtraWhitespace /\s\+$/
          autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
          autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
          autocmd InsertLeave * match ExtraWhitespace /\s\+$/
          autocmd BufWinLeave * call clearmatches()

          set foldmethod=indent
          " set foldnestmax=10
          set foldlevel=0
          " set foldenable

          " disable autocompletion, cause we use deoplete for completion
          let g:jedi#completions_enabled = 1
          " enable deoplete
          " let g:deoplete#enable_at_startup = 1

          " auto close completion window
          autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

          " deoplete tab-complete
          inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

          " open NERDTree on the right side
          let g:NERDTreeWinPos="right"
        '';
      };
    }
  )];
}

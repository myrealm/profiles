{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.gui [
  "chonker"
])
{
  # desktop packages
  environment.systemPackages = with pkgs; [
    pandoc
    virtmanager
    thunderbird
    libreoffice
    remmina
    inkscape
    imagemagick
    audacity
    audacious
    stellarium
    unstable.blender
    obs-studio

    #qt5.full
    #qtcreator
    #python-qt

    #unstable.firefox-wayland
    firefox
    #unstable.ungoogled-chromium

    pam_krb5
    exiftool
    gparted
    element-desktop
    mattermost-desktop
    texlive.combined.scheme-full
    virt-viewer
    ffmpeg-full
    fstl
  ];
}

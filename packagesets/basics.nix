{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.gui [
  "headless"
  "minimal"
  "office"
  "chonker"
])
{

  # basic packages
  environment.systemPackages = with pkgs; [
    bc
    pv
    file
    htop
    wget
    curl
    silver-searcher
    fd
    socat
    graphviz
    mscgen
    dnsutils
    killall
    sysstat
    iftop
    inetutils
    whois
    lsof
    xz
    lz4
    zip
    unzip
    rsync
    tmux
    tree
    usbutils
    nfs-utils
    nmap
    #netcat
    netcat-gnu
    tree
    bmon
    gitMinimal
    fzf
  ];

  environment.variables = {
    FZF_DEFAULT_OPTS = "--height=25% --layout=reverse --info=inline --border --padding=1";
  };

  programs.bash = {
    enableCompletion = true;
    interactiveShellInit  =  ''
      HISTCONTROL=erasedups:ignorespace
      HISTSIZE=-1
      HISTFILESIZE=-1

      if command -v fzf-share >/dev/null; then
        source "$(fzf-share)/key-bindings.bash"
        source "$(fzf-share)/completion.bash"
      fi
    '';
  };
}

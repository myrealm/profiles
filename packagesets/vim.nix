{ pkgs, ... }:
{
  environment.variables = { EDITOR = "vim"; };

  environment.systemPackages = with pkgs; [
    ((vim_configurable.override { python = python3; }).customize {
      name = "vim";
      vimrcConfig.packages.myplugins = with pkgs.vimPlugins; {
        start = [ vim-nix vim-lastplace ];
        opt = [];
      };
      vimrcConfig.customRC = ''
        " the usual
        set nocompatible
        set backspace=indent,eol,start
        set backspace=2 " make backspace work like most other programs

        set viminfo='20,<1000,s1000
        set hlsearch
        set number
        set ruler
        " Tabs are 8 characters, and thus indentations are also 8 characters.
        " There are heretic movements that try to make indentations 4 (or even 2!) characters deep,
        " and that is akin to trying to define the value of PI to be 3.
        set tabstop=8
        set softtabstop=8
        set shiftwidth=8
        set noexpandtab

        " 80 col width marker
        highlight ColorColumn ctermbg=Black ctermfg=DarkRed
        call matchadd('ColorColumn', '\%81v', 100)

        " Highlight trailing spaces
        " http://vim.wikia.com/wiki/Highlight_unwanted_spaces
        highlight ExtraWhitespace ctermbg=red guibg=red
        match ExtraWhitespace /\s\+$/
        autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
        autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
        autocmd InsertLeave * match ExtraWhitespace /\s\+$/
        autocmd BufWinLeave * call clearmatches()

        set foldmethod=indent
        " set foldnestmax=10
        set foldlevel=1
        " set foldenable
      '';
    }
  )];
}

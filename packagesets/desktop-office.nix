{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.gui [
  "office"
])
{
  # minimal additional desktop packages
  environment.systemPackages = with pkgs; [
    firefox
    #chromium
    thunderbird
    libreoffice
    remmina
    inkscape
    imagemagick
    audacity
    audacious
    exiftool
    gparted
    element-desktop
    texlive.combined.scheme-full
    virt-viewer
    ffmpeg-full
  ];
}

{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.gui [
  "minimal"
])
{
  environment.systemPackages = with pkgs; [
    remmina
    virt-viewer
    unstable.firefox
  ];
}

{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.gui [
  "minimal"
  "office"
  "chonker"
])
{
  # Enable SANE and CUPS.
  hardware.sane.enable = true;
  services.printing = {
    enable = true;
    drivers = with pkgs; [ gutenprint ];
  };


  # printing stuffs
  environment.systemPackages = with pkgs; [
    gutenprintBin
    gutenprint
    cups-filters
    canon-cups-ufr2
    cups-bjnp
    sane-backends
    scanbd
  ];
}

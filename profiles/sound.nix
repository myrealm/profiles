{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.gui [
  "minimal"
  "office"
  "chonker"
])
{
  # extra Packages for JACK
  environment.systemPackages = with pkgs; [
  ];

  # Enable Audio Support
  #sound.enable = true;
  hardware.pulseaudio = {
    #enable = true;
    package = pkgs.pulseaudioFull;
  };
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    package = pkgs.pipewire;
    #alsa.enable = true;
    #alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };
}

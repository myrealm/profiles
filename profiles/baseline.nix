{ options, config, pkgs, lib, ... }:
{
  # TODO should probably a per-host setting
  nixpkgs.config = {
    # Allow proprietary packages
    allowUnfree = true;
    # allowBroken = true;
  };

  # unstable nix command with flakes
  nix = {
    package = pkgs.nixVersions.nix_2_17; # redefinition?!
    extraOptions = ''
      connect-timeout = 5
      experimental-features = nix-command flakes
    '';
  };

  systemd.tmpfiles.rules = [
    "d /etc/nixos/secrets 600 root root"
    "d /root/secrets 600 root root"
  ];

  # mount tmpfs on /tmp -> not good for build hosts
  boot.tmp.useTmpfs = lib.mkDefault true;

  #security.apparmor.enable = false;

  # Select internationalisation properties.
  console = {
    font = "Lat2-Terminus16";
  };
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_ALL = "en_US.UTF-8";
    };
  };

  # Set your time zone. TODO: move to "layer"
  time.timeZone = "Europe/Amsterdam";
  # Select internationalisation properties.
  #console = {
  #  keyMap = "de"; # per host setting
  #};

  system.stateVersion = "23.05"; # Did you read the comment?
}

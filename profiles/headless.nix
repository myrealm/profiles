{
  imports = [
    ../packagesets/basics.nix
    ../packagesets/nvim.nix
  ];
}

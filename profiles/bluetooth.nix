{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.gui [
  "minimal"
  "office"
  "chonker"
])
#{ config, pkgs, ... }:
#let
#  unstable = import <nixpkgs-unstable> {};
#in
{
  # Enable bluetooth, add config for A2DP sink?
  hardware.bluetooth = {
    enable = true;
    #package = pkgs.unstable.bluez5-experimental;
    # is this RL?!
    #hsphfpd.enable = true;
    #extraConfig = ''
    #  [General]
    #  Enable=Source,Sink,Media,Socket
    #'';
  };

  #hardware.pulseaudio = {
  #  extraModules = [ pkgs.pulseaudio-modules-bt ];
  #  # fml!
  #  #configFile = pkgs.writeText "default.pa" ''
  #  #  load-module module-switch-on-connect
  #  #  load-module module-bluetooth-policy
  #  #  load-module module-bluetooth-discover
  #  #  ## module fails to load with
  #  #  ##   module-bluez5-device.c: Failed to get device path from module arguments
  #  #  ##   module.c: Failed to load module "module-bluez5-device" (argument: ""): initialization failed.
  #  #  # load-module module-bluez5-device
  #  #  # load-module module-bluez5-discover
  #  #'';
  #};
  #environment.systemPackages = with pkgs; [
  #  #unstable.bluezFull
  #  bluezFull
  #];

  hardware.pulseaudio.enable = false;

  services.pipewire  = {
    # enable pulseaudio emulation
    # e.g. needed for network streaming
    #pulse.enable = true;

    wireplumber.enable = true;
    #media-session.config.bluez-monitor.rules = [
    #  {
    #    # Matches all cards
    #    matches = [ { "device.name" = "~bluez_card.*"; } ];
    #    actions = {
    #      "update-props" = {
    #        # fix for Sony WH-1000XM3
    #        "bluez5.reconnect-profiles" = [ "hfp_hf" "hsp_hs" "a2dp_sink" ];
    #        # mSBC is not expected to work on all headset + adapter combinations.
    #        "bluez5.msbc-support" = true;
    #        # SBC-XQ is not expected to work on all headset + adapter combinations.
    #        "bluez5.sbc-xq-support" = true;
    #      };
    #    };
    #  }
    #  {
    #    matches = [
    #      # Matches all sources
    #      { "node.name" = "~bluez_input.*"; }
    #      # Matches all outputs
    #      { "node.name" = "~bluez_output.*"; }
    #    ];
    #    actions = {
    #      "node.pause-on-idle" = false;
    #    };
    #  }
    #];
  };
}

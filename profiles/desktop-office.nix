{ config, pkgs, ... }:
{
  imports = [
    ./baseline.nix
    ./gnome.nix
    ./bluetooth.nix
    ./sound.nix

    ../packagesets/basics.nix
    ../packagesets/nvim.nix
    ../packagesets/desktop-minimal.nix
    ../packagesets/desktop-office.nix
  ];
}

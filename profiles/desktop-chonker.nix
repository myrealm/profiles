{
  imports = [
    ./gnome.nix
    ./bluetooth.nix
    ./printing.nix
    ./sound.nix

    ../packagesets/basics.nix
    ../packagesets/nvim.nix
    ../packagesets/desktop-minimal.nix
    ../packagesets/desktop-office.nix
    ../packagesets/desktop-fat.nix
    ../packagesets/dev.nix
    ../packagesets/dev-electronix.nix
    ../packagesets/dev-python.nix
  ];
}

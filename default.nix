{
  # all
  imports = [
    ./profiles/baseline.nix
    ./profiles/desktop-chonker.nix
    ./services/client-dhcp.nix
    ./services/resizefs.nix
    ./hardware/qemu
    ./hardware/hetzner
    ./profiles/headless.nix
  ];
}

{ config, lib, pkgs, modulesPath, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.hardware [
  "qemu-bios"
  "qemu-efi"
])
{
  boot.initrd.postDeviceCommands =
    ''
      # Set the system time from the hardware clock to work around a
      # bug in qemu-kvm > 1.5.2 (where the VM clock is initialised
      # to the *boot time* of the host).
      hwclock -s
    '';

  boot.initrd.availableKernelModules = [ "virtio_net" "virtio_pci" "virtio_mmio" "virtio_blk" "virtio_scsi" "9p" "9pnet_virtio" "ahci" "xhci_pci" "sr_mod" ];
  boot.initrd.kernelModules = [ "virtio_balloon" "virtio_console" "virtio_rng" ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  fileSystems."/" = { device = "/dev/vda1"; fsType = "ext4"; };

  swapDevices = [ ];
}

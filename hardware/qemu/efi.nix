{ config, lib, pkgs, modulesPath, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.hardware [
  "qemu-efi"
])
{
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
}

{
  imports = [
    ./common.nix
    ./efi.nix
    ./bios.nix
  ];
}

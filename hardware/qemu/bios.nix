{ config, lib, pkgs, modulesPath, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.hardware [
  "qemu-bios"
])
{
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda";
}

{
  description = "Packagesets and profiles";

  inputs = {
    #pkgs = { url = "gitlab:myrealm/packages/main"; };
    nixpkgs = { url = "gitlab:myrealm/packages/main"; };
  };

  outputs = { self, nixpkgs, ... }@inputs:
  {
    nixosModules.default = { config, lib, nixpkgs, ... }:
    with lib;
    let
      hw = [
        "nixos-container"
        "qemu-bios"
        "qemu-efi"
        "hetzner"
        "desktop-bios"
        "desktop-efi"
      ];
      ui = [
        "headless"
        "minimal"
        "office"
        "chonker"
      ];
      gpu = [
        "nvidia-amd-desktop"
      ];
      sv = [
        "client-dhcp"
        "ssh-only"
      ];
      cfg = config.myrealm.profile;
    in
    {
      imports = [
        # infinite recursion
          #({ config, pkgs, ... }:
          #{
          #  nixpkgs.overlays = [ pkgs.overlays.default ];
          #})
        ./default.nix
      ];
      options.myrealm.profile = with types; {
        enable = mkEnableOption "Enable Profiles";
        hardware = mkOption {
          type = nullOr (enum hw);
          default = null;
        };
        gui = mkOption {
          type = nullOr (enum ui);
          default = "headless";
        };
        gpu = mkOption {
          type = nullOr (enum gpu);
          default = null;
        };
        service = mkOption {
          type = nullOr (enum sv);
          default = "ssh-only";
        };
        resizefs = mkOption {
          type = bool;
          default = false;
        };
        local = mkOption {
          type = nullOr (listOf str);
          default = null;
        };
      };
      config = mkIf cfg.enable {
        systemd.services.profile-greeter = {
          wantedBy = [ "multi-user.target" ];
          serviceConfig.Type = "oneshot";
          #serviceConfig.ExecStart = ''
          #  ${pkgs.hello}/bin/hello -g'Your profile(s): "${escapeShellArg cfg.hardware}"'
          #'';
          script = ''
            echo "#########################################"
            echo "   ### Your active profiles are:"
            echo "#########################################"
            echo "   ### Hardware: ${escapeShellArg cfg.hardware}"
            echo "   ### UX: ${escapeShellArg cfg.gui}"
            echo "   ### GPU: ${escapeShellArg cfg.gpu}"
            echo "   ### Service: ${escapeShellArg cfg.service}"
            echo "#########################################"
          '';
        };
      };
    };
  };
}

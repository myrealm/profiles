{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.service [
  "client-dhcp"
])
{
  # Networking.
  networking.useDHCP = lib.mkDefault true;
  networking.firewall.enable = true;
}

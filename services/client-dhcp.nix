{
  imports = [
    ./dhcp-with-fw.nix
    ./ntp.nix
    ./resolved.nix
    ./ssh.nix
  ];
}

{ pkgs, lib, config, ... }:
let
  ntpservers = [
      "0.de.pool.ntp.org"
      "1.de.pool.ntp.org"
      "2.de.pool.ntp.org"
      "3.de.pool.ntp.org"
    ];
in
lib.mkIf (builtins.elem config.myrealm.profile.service [
  "client-dhcp"
  "ssh-only"
])
{
  services.chrony = {
    enable = true;
    servers = ntpservers;
    #initstepslew = {
    #  enabled = true;
    #  threshold = 1000;
    #};
  };

  services.openntpd = {
    enable = false;
    servers = ntpservers;
    #extraOptions = "-s"; #deprecated
  };

  services.timesyncd = {
    enable = false;
    servers = ntpservers;
  };
}

{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.service [
  "client-dhcp"
  "ssh-only"
])
{

  networking.firewall.allowedTCPPorts = [ 22 ];
  services.openssh =  {
    enable = true;
    # Only pubkey auth
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
    };
  };
}

{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.service [
  "client-dhcp"
  "ssh-only"
])
{
  services.resolved = {
    enable = true;
    #domains = [ "lan" "slowfi" "nanolan" "ipc.uni-tuebingen.de" "living-systems.dev" ];
    llmnr = "false";
    #fallbackDns = [ "84.200.69.80" "84.200.70.40" ]; # DNS.WATCH
    #fallbackDns = [ "8.8.8.8" "8.8.4.4" ]; # Google
    # Cloudflare
    fallbackDns = [
      "1.1.1.1"
      "2606:4700:4700::1111"
      "1.0.0.1"
      "2606:4700:4700::1001"
    ];

    # workaround for https://github.com/NixOS/nixpkgs/issues/66451
    dnssec = "false";

    extraConfig = ''
      # might help if there is a local nameserver instance
      DNSStubListener=no
      #DNSOverTLS=opportunistic # *good* for captive portals
      Cache=no
      #MulticastDNS=no
      #ReadEtcHosts=yes
    '';
  };
}

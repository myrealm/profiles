{ pkgs, lib, config, ... }:
lib.mkIf (builtins.elem config.myrealm.profile.gpu [
  "nvidia"
])
{
  # nonfree nvidia drivers
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.nvidia.powerManagement.enable = true; # maybe?!
}
